/*


#define PIN_WIRE_SDA (4)
#define PIN_WIRE_SCL (5)

static const uint8_t SDA = PIN_WIRE_SDA;
static const uint8_t SCL = PIN_WIRE_SCL;

static const uint8_t LED_BUILTIN = 16;
static const uint8_t BUILTIN_LED = 16;

static const uint8_t D0   = 16;
static const uint8_t D1   = 5;
static const uint8_t D2   = 4;
static const uint8_t D3   = 0;
static const uint8_t D4   = 2;
static const uint8_t D5   = 14;
static const uint8_t D6   = 12;
static const uint8_t D7   = 13;
static const uint8_t D8   = 15;
static const uint8_t D9   = 3;
static const uint8_t D10  = 1;

Ref:
https://randomnerdtutorials.com/esp8266-pinout-reference-gpios/

*/

//#define DEC_LIMIT D1
#define HEARTBEAT_LED 2
//#define ASC_LED_LIMIT D3
#define ROTATION_STEP_PIN 5
#define ROTATION_STEP_DIR 18
#define ANGLE_STEP_PIN 22
#define ANGLE_STEP_DIR 23
#define PARK 39
#define ANGLE_LIMIT 35
#define TRACK_ON 36
#define GO_HOME 34

#define LED_HOMED 32
#define LED_TRACK 12
#define LED_LIMIT 13
#define LED_FAULT 16
#define LED_MOVE 26



//#define HOME_SWITCH D7
//#define CMD_ACTIVE 2
