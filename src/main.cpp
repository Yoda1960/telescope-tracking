#include <Arduino.h>
#include <ArduinoOTA.h>
#include <FreeRTOS.h>
#include <PubSubClient.h>
#include <WiFi.h>
#include <WiFiUdp.h>

#include "globals.h"
#include <commands.h>
//#include <lcd.h>
#include "taskdata.h"
#include <led.h>
#include <limitSwitch.h>
#include <lx200.h>
#include <motors.h>
#include <network.h>


QueueHandle_t queue;
SemaphoreHandle_t xSerialSemaphore = NULL;
SemaphoreHandle_t xMQTTSemaphore = NULL;

bool lBooted;
unsigned int Distance = 0; // Record the number of steps we've taken

char input[20];

bool stringComplete;
String inputString = ""; // a string to hold incoming data
int zz = 0;
int mode = 0;
int lNorthIsSet = false;
int flashCount = 0;
boolean ToggleFlash = false;

//#define DEBUG
bool lTrack;
// bool lHomed = false;
bool isParkAsked = false;
bool isHomeAsked = false;

void SerialOut(const char *text) {

  if (xSemaphoreTake(xSerialSemaphore, (TickType_t)10) == pdTRUE) {
    Serial.println(text);
    xSemaphoreGive(xSerialSemaphore);
  }
}

void setup() {
  lBooted = true;
  Serial.begin(115200);
  xSerialSemaphore = xSemaphoreCreateMutex();
  xMQTTSemaphore = xSemaphoreCreateMutex();


  /*
   *Only for testing so don't need to press North Button
   */
  SaveRotationStepPosition(0);
  // setupLCD();
  setupNetwork();
  StartFIFO();
  connectMQTT();

  CreateMQTTQueue();
  CreateTasks();


  stringComplete = false;
  setupLED();
  setUpSwitchs();
  setupMotors();
  lTrack = TrackingOn();
  // lcdTracking(lTrack);
  // lcdHomed(HasHomeBeenSet());


  //mqttInfo("Start", __LINE__, __FILE__);

  SerialOut("Booted");
  OutputTaskData();
  mqttStatus("Online");
  //MotorMessage(cmdHOME, 0, 0);
}

void loop() {
  OTA();
  connectMQTT();
  
  if (lTrack != TrackingOn()) {
    lTrack = TrackingOn();
    if (lTrack) {
      mqttInfo("Trackin On", __LINE__, __FILE__);
    } else {
      mqttInfo("Trackin Off", __LINE__, __FILE__);
    }
  }

  if (lTrack) {
    trackLED_on();
    //if (isCommandAvailable()) {
    //  cmdLED_on();
    //  DoCommand();
    //  mqttDebug("DoCmd Complete", __LINE__, __FILE__);
    //  OutputTaskData();
    //  cmdLED_off();
    //}

  } else {
    trackLED_off();
    if (GoHome()) {
      // lcdLastCmd("Find Home");
      MotorMessage(cmdHOME, 0, 0);
      // lcdLastCmd("At Home");
      // lcdHomed(HasHomeBeenSet());
    }
    /*
     * can only park if we know where home is
     */
    if (HasHomeBeenSet()) {
      if (DoPark()) {
        // lcdLastCmd("Park");
        Park();
      }
    }
  }

  return;
}