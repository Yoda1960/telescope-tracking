#include <Arduino.h>
#include <led.h>
#include <pins.h>

extern QueueHandle_t queue;

void setupLED() {

  pinMode(LED_HOMED, OUTPUT);
  pinMode(LED_TRACK, OUTPUT);
  pinMode(LED_LIMIT, OUTPUT);
  pinMode(LED_FAULT, OUTPUT);
  pinMode(LED_MOVE, OUTPUT);
  pinMode(HEARTBEAT_LED, OUTPUT);

  homedLED_off();
  trackLED_off();
  limitLED_off();
  faultLED_off();
  cmdLED_off();

  // pinMode(ASC_LED_LIMIT, OUTPUT);
  // pinMode(CMD_ACTIVE, OUTPUT);
}
void hearbeatLED_on() { digitalWrite(HEARTBEAT_LED, HIGH); }

void heartbeatLED_off() { digitalWrite(HEARTBEAT_LED, LOW); }

void homedLED_on() { digitalWrite(LED_HOMED, LOW); }
void trackLED_on() { digitalWrite(LED_TRACK, LOW); }
void limitLED_on() { digitalWrite(LED_LIMIT, LOW); }
void faultLED_on() { digitalWrite(LED_FAULT, LOW); }
void cmdLED_on() { digitalWrite(LED_MOVE, LOW); }
void homedLED_off() { digitalWrite(LED_HOMED, HIGH); }
void trackLED_off() { digitalWrite(LED_TRACK, HIGH); }
void limitLED_off() { digitalWrite(LED_LIMIT, HIGH); }
void faultLED_off() { digitalWrite(LED_FAULT, HIGH); }
void cmdLED_off() { digitalWrite(LED_MOVE, HIGH); }

void heartbeatTask(void *pvParameter) {

  for (;;)
  {
    /* Blink off (output low) */
    hearbeatLED_on();
    vTaskDelay(1000 / portTICK_RATE_MS);
    /* Blink on (output high) */
    heartbeatLED_off();
    vTaskDelay(2000 / portTICK_RATE_MS);
  }
}