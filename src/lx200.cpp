#include <Arduino.h>
#include <PubSubClient.h>
#include <lx200.h>

/*
 * This code was based on work from
 * https://github.com/graphworlok/Equatorial_Basic_RA_DEC_LX200_Driver
 */


char txAR[10];
char txDEC[11];

extern PubSubClient client;


void parseLX200(char *input)
{
  sprintf(txAR, "%02d:%02d:%02d#", 06, 23, 57);
  sprintf(txDEC, "%c%02d%c%02d:%02d#", 45, 52, '*', 41, 43);

  if (input[1] == ':' && input[2] == 'G' && input[3] == 'R' && input[4] == '#')
  {
    //client.publish("scope/AR", txAR);
    Serial.print(txAR);
    return;
  }

  if (input[1] == ':' && input[2] == 'G' && input[3] == 'D' && input[4] == '#')
  {
    //client.publish("scope/DEC", txDEC);
    Serial.print(txDEC);
    return;
  }
  //Synchronizes the telescope's position with the currently selected database object's coordinates
  if (input[0] == ':' && input[1] == 'C' && input[2] == 'M' && input[3] == '#')
  {
    Serial.print("0#");
    client.publish("scope/Sync", input);
    return;
  }
  if (input[0] == ':' && input[1] == 'M' && input[2] == 'S' && input[3] == '#')
  {
    Serial.print("0");
    client.publish("scope/Slew", input);
    return;
  }
  //Set target object RA
  if (input[0] == ':' && input[1] == 'S' && input[2] == 'r')
  {
    Serial.print("1");
    client.publish("scope/SetRA", input);
    return;
  }
  //Set target object declination
  if (input[0] == ':' && input[1] == 'S' && input[2] == 'd')
  {
    Serial.print("1");
    client.publish("scope/SetDec", input);
    return;
  }

  //Halt all current slewing
  if (input[0] == '#' && input[1] == ':' && input[2] == 'Q' && input[3] == '#')
  {
    client.publish("scope/Stop", input);
    return;
  }
  //Dump out other unhandled
  client.publish("scope/OTHER", input);
} // Ending Function
