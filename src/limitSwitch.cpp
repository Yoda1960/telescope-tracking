#include <Arduino.h>
#include <limitSwitch.h>
#include <pins.h>

bool varHomeIsSet = false;

void setUpSwitchs() {
  pinMode(TRACK_ON, INPUT);
  pinMode(PARK, INPUT);
  pinMode(GO_HOME, INPUT);
  pinMode(ANGLE_LIMIT, INPUT);

  // pinMode(ASC_LIMIT, INPUT_PULLUP);
  // pinMode(HOME_SWITCH, INPUT_PULLUP);
}

bool HasHomeBeenSet() { return varHomeIsSet; }
void HomeIsSet() { varHomeIsSet = true; }
bool TrackingOn() { return (digitalRead(TRACK_ON) < 1); }
/*
 * can only go home or park if tracking is off
 */
bool GoHome() {
  if (!TrackingOn()) {
    return (digitalRead(GO_HOME) < 1);
  }
  return false;
}
bool DoPark() {
  if (!TrackingOn()) {
    return (digitalRead(PARK) < 1);
  }
  return false;
}
bool IsLimit() { return (digitalRead(ANGLE_LIMIT) > 0); }