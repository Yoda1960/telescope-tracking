void setupLED();
void hearbeatLED_on();
void heartbeatLED_off();
void cmdLED_on();
void homedLED_on();
void trackLED_on();
void limitLED_on();
void faultLED_on();
void cmdLED_off();
void homedLED_off();
void trackLED_off();
void limitLED_off();
void faultLED_off();

void heartbeatTask(void *pvParameter);