

void setupNetwork();
void connectMQTT();
void mqttTask(void *pvParameter);
void mqttInfo(const char *text, int line, const char *file);
void mqttDebug(const char *text, int line, const char *file);
void mqttStatus(const char *text);
void mqttAction(const char *text);
void OTA();
void mqttLoop();
void CreateMQTTQueue();

enum { mqttSTATUS, mqttACTION, mqttINFO, mqttERROR, mqttDEBUG };