#include <Arduino.h>
#define CMDS_H

#include "globals.h"
#include "motors.h"
#include "taskdata.h"
#include <commands.h>
#include <cppQueue.h>
#include <motors.h>
#include <network.h>

#define IMPLEMENTATION FIFO
#define OVERWRITE true

Queue q(sizeof(sCommand), 10, IMPLEMENTATION, OVERWRITE);

void StartFIFO() {
  // Q.begin();
  String msg = "FIFO Started : Size " + q.sizeOf();
  mqttInfo(msg.c_str(), __LINE__, __FILE__);
}
/*
 * Check any commands available to action
 */
boolean isCommandAvailable() { return !q.isEmpty(); }

void ProcessMessage(char *msg) {
  unsigned int i;
  if (strncmp(msg, "CMD", 3) == 0) {
    mqttDebug("Command arrived", __LINE__, __FILE__);
    mqttDebug(msg, __LINE__, __FILE__);
    sCommand aCommand;
    /****
     * Stop the warnings for complier
     ****/
    aCommand.angle = 0;
    aCommand.rotation = 0;
    if (msg[4] == 'M' || msg[4] == 'T') {
      char *str;
      char *p = msg;
      i = 0;
      while ((str = strtok_r(p, ":", &p)) != NULL) {
        if (i == 2) {
          aCommand.angle = atof(str);
        }
        if (i == 3) {
          aCommand.rotation = atof(str);
        }
        i++;
      }
      // aCommand.sequence = cmdCount;
      // aCommand.id = cmdMOVE;
      // q.push(&aCommand);
      switch (msg[4]) {
      case 'M':
        mqttDebug("Move", __LINE__, __FILE__);
        MotorMessage(cmdMOVE, aCommand.rotation, aCommand.angle);
        break;
      case 'T':
        mqttDebug("Trim", __LINE__, __FILE__);
        MotorMessage(cmdTRIM, aCommand.rotation, aCommand.angle);
        break;
      }

    } else if (msg[4] == 'S') {
      mqttDebug("Step", __LINE__, __FILE__);
      // TBA MotorMessage(cmdSTEP,aCommand.rotation,aCommand.angle);
    } else if (msg[4] == 'T') {
      mqttDebug("Trim", __LINE__, __FILE__);
      MotorMessage(cmdTRIM, aCommand.rotation, aCommand.angle);
    } else if (msg[4] == 'H') {
      // mqttDebug("Home", __LINE__, __FILE__);
      // sCommand aCommand;
      // aCommand.sequence = cmdCount;
      // aCommand.id = cmdHOME;
      // q.push(&aCommand);
      MotorMessage(cmdHOME, 0, 0);
    } else {
      mqttInfo("Unknown Command arrived", __LINE__, __FILE__);
    }
  } else {
    mqttDebug("Ignored", __LINE__, __FILE__);
    mqttDebug(msg, __LINE__, __FILE__);
  }
}
/* Not used
void DoCommand() {

  String thisString =
      String("Heap:") + String(xPortGetMinimumEverFreeHeapSize());
  mqttDebug(thisString.c_str(), __LINE__, __FILE__);

  sCommand aCommand;
  q.pop(&aCommand);

  thisString = String("Command Sequence:") + String(aCommand.sequence);
  mqttDebug(thisString.c_str(), __LINE__, __FILE__);
  // lcdRotation(aCommand.atz);
  // lcdAngle(aCommand.at);
  switch (aCommand.id) {
  case cmdMOVE:
    Move(aCommand.rotation, aCommand.angle, false, false);
    mqttDebug("Move Completed", __LINE__, __FILE__);
    break;
  case cmdHOME:
    // Home();
    break;
  case cmdDATA:
    OutputTaskData();
    break;
  default:
    mqttDebug("Ignored", __LINE__, __FILE__);
  }
}
*/