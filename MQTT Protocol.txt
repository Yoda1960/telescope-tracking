MQTT Topics
scope/info: Results of commands post back.
scope/cmd : Commands for processings

Command posted to MQTT server:
XXX:Y:AAAA:BBBB

XXX
CMD : All messages must start with this
Y
M : Move to the absolute rotation and angle
S : Move AAAA/BBBB steps 
T : Move AAAA/BBBB steps but do not store movement, to trim position.
F : Adjust the Steps per rotation for A/R and save to flash 
V : No other inputs , will dump the currents step postion, homed, steps setting for A/R (default and flash) to info topic, stack highwater markc
H : Find home
Pins:
02 - Hearbeat LED (internal)
36 - Track On Switch 
35 - Limit Switch
39 - Park Toggle
34 - Goto Limit Toggle

32 - Limit Found LED
12 - Track On LED
13 - Limit LED
26 - Fault LED
25 - Move LED

17 - Trim Angle: 2 Pins
18 -
27 - Trim Rotation: 2 Pins
14 -

23 - Stepper Angle : 2 Pins
22 -
18 - Stepper Rotation : 2 Pins
05 -


28 src\motors.cpp       : Save New Rotation Position -67916 
 34 src\motors.cpp       : Save New Angle Position 0 
136 src\network.cpp      : System booted 
140 src\network.cpp      : connect - IP Address:192.168.30.3 
 27 src\commands.cpp     : Command arrived 
 28 src\commands.cpp     : CMD:M:-55.99:-106.54 
 31 src\commands.cpp     : Move 
 69 src\commands.cpp     : Command Sequence:0 
 85 src\motors.cpp       : Start Move -55.99/-106.54 
