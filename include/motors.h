#include "globals.h"

// void xHome();
void setupMotors();
bool Move(float rotatin, float angle, bool isPark, bool isTrim);
void Park();
void SaveRotationStepPosition(long pos);
void SaveAngleStepPosition(long pos);
#define FULL_ROTATE_STEPS (57400 * 4)
#define FULL_ANGLE_STEPS 32500
void motorTask(void *pvParameter);
void MotorMessage(cmdRecieved CMD, float Rotation, float Angle);