#include <Arduino.h>
#include <FreeRTOS.h>

#include "led.h"
#include "motors.h"
#include "network.h"
#include "taskdata.h"

#define T_HANDLE 0
#define T_SIZE 1
#define T_NAME 2
#define MAX_TASKS 3

TaskHandle_t startedTasks[MAX_TASKS];
String startedTasksNames[MAX_TASKS];
UBaseType_t uxHighWaterMark;
int TaskAllocated = 0;

void StoreTask(int id, TaskHandle_t hand, const char *name) {
  startedTasks[id] = hand;
  startedTasksNames[id] = name;
  TaskAllocated++;
}

void CreateTasks() {
  TaskHandle_t pvCreatedTask;
  xTaskCreate(&heartbeatTask, "blinky", 700, NULL, 5, &pvCreatedTask);
  StoreTask(0, pvCreatedTask, "blinky");
  xTaskCreate(&motorTask, "motors", 2000, NULL, 5, &pvCreatedTask);
  StoreTask(1, pvCreatedTask, "motor");
  xTaskCreate(&mqttTask, "mqtt", 1500, NULL, 5, &pvCreatedTask);
  StoreTask(2, pvCreatedTask, "mqtt");
}

void OutputTaskData() {

  char buffer[128];
  UBaseType_t uxHighWaterMark;
  snprintf(buffer, 127, "+++++++ Heap  %14s ++++++++",
           String(xPortGetMinimumEverFreeHeapSize()).c_str());
  mqttDebug(buffer, __LINE__, __FILE__);

  for (int x = 0; x < MAX_TASKS && x < TaskAllocated; x++) {
    /* Inspect our own high water mark on entering the task. */
    uxHighWaterMark = uxTaskGetStackHighWaterMark(startedTasks[x]);
    snprintf(buffer, 127, "+++++++ Stack %10s %4d ++++++++",
             startedTasksNames[x].c_str(), uxHighWaterMark);
    mqttDebug(buffer, __LINE__, __FILE__);
  }
}