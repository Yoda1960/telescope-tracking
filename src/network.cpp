#include <Arduino.h>
#include <ArduinoOTA.h>
#include <PubSubClient.h>
#include <WiFi.h>
#include <WiFiUdp.h>

#include "globals.h"
#include "network.h"
#include <commands.h>

extern SemaphoreHandle_t xMQTTSemaphore;
QueueHandle_t xMQTTQueue = NULL;
#define xMQTTQueueMaxMessage 100

struct xMQTT {
  int type;
  char str[xMQTTQueueMaxMessage];
};

//#define DEBUG

WiFiClient espClient;
PubSubClient client(espClient);

IPAddress ip(192, 168, 30, 3);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 192, 0);
IPAddress dns(192, 168, 1, 1);

const char *ssid = "Crysis_G";
// const char *ssid = "Crysis";
const char *password = "slowater";
// Old MQTT const char *mqtt_server = "192.168.2.31";
const char *mqtt_server = "192.168.20.1";
const char *mqtt_user = "node";
const char *mqtt_password = "password";
char Address[20];
extern bool lBooted;

void CreateMQTTQueue() {
  if (xMQTTQueue == NULL) {
    xMQTTQueue = xQueueCreate(20, sizeof(struct xMQTT));
    if (xMQTTQueue == NULL) {
      SerialOut("Created MQQT Queue FAILED");
    } else {
      SerialOut("Created MQQT Queue");
    }
  }
}
void mqttOuts(const char *text, int line, const char *file,
              bool debug = false) {
  char str[xMQTTQueueMaxMessage];
  if (xSemaphoreTake(xMQTTSemaphore, (TickType_t)10) == pdTRUE) {
    if (!debug) {
      snprintf(str, xMQTTQueueMaxMessage - 1, "%s : Line:%d File:%s", text,
               line, file);
      client.publish("scope/info", str);

    } else {
      snprintf(str, xMQTTQueueMaxMessage - 1, "%3d %-20s : %s ", line, file,
               text);
      client.publish("scope/debug", str);
    }

    xSemaphoreGive(xMQTTSemaphore);
  }
}

void mqttInfo(const char *text, int line, const char *file) {
  if (xMQTTQueue == NULL) {
    SerialOut("MQTT queue not ready");
    return;
  }
  /*
   * Info also writes debug
   */
  struct xMQTT msg;
  msg.type = mqttINFO;
  // snprintf(msg.str, xMQTTQueueMaxMessage - 1, "%3d %-20s : %s ", line,
  // file,text);
  snprintf(msg.str, xMQTTQueueMaxMessage - 1, "%s ", text);
  xQueueSend(xMQTTQueue, &msg, portMAX_DELAY);

  msg.type = mqttDEBUG;
  xQueueSend(xMQTTQueue, &msg, portMAX_DELAY);

  // mqttOut(text, line, file, false);
  //  mqttOut(text, line, file, true);
}

void mqttAction(const char *text) {
  if (xMQTTQueue == NULL) {
    SerialOut("MQTT queue not ready");
    return;
  }
  struct xMQTT msg;
  msg.type = mqttACTION;
  snprintf(msg.str, xMQTTQueueMaxMessage - 1, "%s ", text);
  xQueueSend(xMQTTQueue, &msg, portMAX_DELAY);

}

void mqttStatus(const char *text) {
  if (xMQTTQueue == NULL) {
    SerialOut("MQTT queue not ready");
    return;
  }
  struct xMQTT msg;
  msg.type = mqttSTATUS;
  snprintf(msg.str, xMQTTQueueMaxMessage - 1, "%s ", text);
  xQueueSend(xMQTTQueue, &msg, portMAX_DELAY);

}

void mqttDebug(const char *text, int line, const char *file) {
  if (xMQTTQueue == NULL) {
    SerialOut("MQTT queue not ready");
    return;
  }
  /*
   * debug is just infor with flag
   */

  struct xMQTT msg;
  msg.type = mqttDEBUG;
  snprintf(msg.str, xMQTTQueueMaxMessage - 1, "%3d %-20s : %s ", line, file,
           text);
  xQueueSend(xMQTTQueue, &msg, portMAX_DELAY);
  // mqttOut(text, line, file, true);
}

void setup_wifi() {
#ifdef DEBUG
  Serial.println("Setup WiFi");
#endif

  delay(10);
  WiFi.setHostname("esp32Scope");
  WiFi.config(ip, gateway, subnet, dns);
  WiFi.begin(ssid, password);
  int RetryCount = 0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    RetryCount++;
    // If been trying for 30 secs and no wifi then reboot
    if (RetryCount > 60) {
      // resetFunc();
    }
  }
  randomSeed(micros());
  // Store my IP for latter
  sprintf(&Address[0], "%s", WiFi.localIP().toString().c_str());
#ifdef DEBUG
  Serial.print("Address:");
  Serial.println(Address);
#endif
  ArduinoOTA.onEnd([]() { Serial.println("\nEnd"); });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });
  ArduinoOTA.begin();
}

void OTA() { ArduinoOTA.handle(); }
void setupNetwork() {
#ifdef DEBUG
  Serial.println("Setup Network");
#endif
  setup_wifi();
  WiFi.softAPdisconnect(true);
  client.setServer(mqtt_server, 1883);
#ifdef DEBUG
  Serial.println("Setup Network Compete");
#endif
}

void mqttLoop() { client.loop(); }

void callback(char *topic, byte *payload, unsigned int length) {
  char msg[100];
  /*
   * ensure messge does not over run buffer and leav spase for terminating
   * string
   */
  if (length > 99) {
    length = 99;
  }
  unsigned int i = 0;
  for (i = 0; i < length; i++) {
    msg[i] = ((char)payload[i]);
  }
  msg[i] = '\0';
  ProcessMessage(msg);
}

void connectMQTT() {
  String msg;
  if (!client.connected()) {
    // Loop until we're reconnected
    while (!client.connected()) {
      // Create a random client ID
      String clientId = "SCOPE-";
      clientId += String(random(0xffff), HEX);
      // Attempt to connect
      if (client.connect(clientId.c_str(), mqtt_user, mqtt_password,
                         "scope/status-ctl", 0, 0, "Offline")) {
        // Once connected, publish an announcement...
        if (lBooted) {
          // mqttInfo("###### System booted #######", __LINE__, __FILE__);
        } else {
          // mqttInfo("###### Reconnected  #######", __LINE__, __FILE__);
        }
        msg = "connect - IP Address:";
        msg += Address;
        client.subscribe("scope/cmd");
        client.setCallback(callback);
        mqttInfo(msg.c_str(), __LINE__, __FILE__);
        mqttStatus("Online");
      } else {
        // Wait 5 seconds before retrying
        delay(5000);
      }
    }
    mqttStatus("Online");
  }
  client.loop();
  if (lBooted) {
    lBooted = false;
  }
}

void mqttTask(void *pvParameter) {

  while (xMQTTQueue == NULL) {
    SerialOut("MQTT Queue Task not ready");
    vTaskDelay(2000 / portTICK_RATE_MS);
  }

  struct xMQTT message;

  for (;;) {
    if (xQueueReceive(xMQTTQueue, &(message),
                      (TickType_t)(3000 / portTICK_RATE_MS)) == pdPASS) {
      /* xRxedStructure now contains a copy of xMessage. */
      if (message.type == mqttINFO) {
        client.publish("scope/info", &message.str[0]);
      }
      if (message.type == mqttDEBUG) {
        client.publish("scope/debug", &message.str[0]);
      }
      if (message.type == mqttERROR) {
        client.publish("scope/error", &message.str[0]);
      }
      if (message.type == mqttSTATUS) {
        client.publish("scope/status-ctl", &message.str[0]);
      }
      if (message.type == mqttACTION) {
        client.publish("scope/action", &message.str[0]);
      }
    }
    vTaskDelay(100 / portTICK_RATE_MS);
  }
}
