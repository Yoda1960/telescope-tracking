#include "globals.h"
#include <Arduino.h>

typedef struct stCd {
  int sequence;
  float rotation;
  float angle;
  cmdRecieved id;
} sCommand;

#define MAXCMD 10

#ifdef CMDS_H
volatile int cmdCount = 0;
#else
extern volatile int cmdCount;
#endif

void ProcessMessage(char *msg);
boolean isCommandAvailable();
// Not Used void DoCommand();
void StartFIFO();