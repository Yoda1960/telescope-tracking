#include "globals.h"
#include "led.h"
#include "taskdata.h"
#include <Arduino.h>
#include <limitSwitch.h>
#include <motors.h>
#include <network.h>
#include <pins.h>

long CurrentRotationStep;
long CurrentAngleStep;
bool lLevelSet = false;

QueueHandle_t xMotorQueue = NULL;

struct motorMessage {
  cmdRecieved MessageID;
  float ucAngle;
  float ucRotation;
};

void SerialOut(const char *text);

bool IsLevelSet() { return lLevelSet; }

void setupMotors() {
  pinMode(ROTATION_STEP_DIR, OUTPUT);
  pinMode(ROTATION_STEP_PIN, OUTPUT);
  pinMode(ANGLE_STEP_DIR, OUTPUT);
  pinMode(ANGLE_STEP_PIN, OUTPUT);

  digitalWrite(ROTATION_STEP_DIR, LOW);
  digitalWrite(ROTATION_STEP_PIN, LOW);
  digitalWrite(ANGLE_STEP_DIR, LOW);
  digitalWrite(ANGLE_STEP_PIN, LOW);

  xMotorQueue = xQueueCreate(10, sizeof(struct motorMessage));

  if (xMotorQueue == NULL) {
    SerialOut("Motor Queue Failed");
  } else {
    SerialOut("Motor Queue Created");
  }
}

void SaveRotationStepPosition(long pos) {
  CurrentRotationStep = pos;
  String message = "Save New Rotation Position ";
  message += pos;
  mqttDebug(message.c_str(), __LINE__, __FILE__);
}
void SaveAngleStepPosition(long pos) {
  CurrentAngleStep = pos;
  String message = "Save New Angle Position ";
  message += pos;
  mqttDebug(message.c_str(), __LINE__, __FILE__);
}

long GetRotationStepPosition() { return CurrentRotationStep; }
long GetAngleStepPosition() { return CurrentAngleStep; }

/*
 * travel home to see where our position is
 */
void xHome() {
  mqttInfo("Home Request", __LINE__, __FILE__);
  // Serial.println("Go Home");
  // CommandOn();
  digitalWrite(ANGLE_STEP_DIR, HIGH);

  while (!IsLimit()) {
    digitalWrite(ANGLE_STEP_PIN, HIGH);
    delayMicroseconds(1870);
    vTaskDelay(1 / portTICK_RATE_MS);
    digitalWrite(ANGLE_STEP_PIN, LOW);
    delayMicroseconds(1870);
    // vTaskDelay(1870 / portTICK_RATE_MS);
    // yield();
    // mqttLoop();
  }
  mqttDebug("Limit Found", __LINE__, __FILE__);
  // LimitOn();
  // CommandOff();
  HomeIsSet();

  /*
   * As home is 90deg then set the store value to be that
   */
  SaveAngleStepPosition(FULL_ANGLE_STEPS / 4);
  // lcdLastCmd("To 0:0");
  // mqttDebug("To 0:0", __LINE__, __FILE__);
  // LimitOff();
  // delay(200);
  MotorMessage(cmdMOVE, 0, 0);
  // Move(0, 0, false);
  // mqttInfo("Home Done", __LINE__, __FILE__);
}

void Park() {
  mqttInfo("Park", __LINE__, __FILE__);
  Move(0, -10, true, false);
}
/****
 * Move specifc degrees, if in trim mode then postion relative to home is not
 *saved
 ****/
bool Move(float rotation, float angle, bool isPark, bool isTrim) {
  String message;
  message = "Start Move ";
  message += angle;
  message += "/";
  message += rotation;
  if (isTrim) {
    message += " Trim";
  }
  mqttInfo(message.c_str(), __LINE__, __FILE__);

  /****
   * If trim mode move even without home set
   ****/
  if (!HasHomeBeenSet() && !isTrim) {
    mqttInfo("Home is NOT SET", __LINE__, __FILE__);
    return false;
  }

  /****
   * Set the current pos to zero for the case of trim
   * If not trim the fetch the current step position
   ****/
  long RotationStepNow = 0;
  long AngleStepNow = 0;
  if (!isTrim) {
    RotationStepNow = GetRotationStepPosition();
    AngleStepNow = GetAngleStepPosition();
  }

  message = "Current Step ";
  message += RotationStepNow;
  message += "/";
  if (angle >= 0 && angle <= 90) {
    message += AngleStepNow;
    mqttDebug(message.c_str(), __LINE__, __FILE__);
  } else if (angle > 90) {
    angle = 90;
    message += "Too High";
    mqttDebug(message.c_str(), __LINE__, __FILE__);

  } else {
    angle = 0;
    message += "Below Horizon";
    mqttInfo(message.c_str(), __LINE__, __FILE__);
  }

  // CommandOn();
  long NewRotationStepPosition = FULL_ROTATE_STEPS * (rotation / 360.0f);
  long NewAngleStepPosition = FULL_ANGLE_STEPS * (angle / 360.0f);
  message = "Postiton Required ";
  message += NewRotationStepPosition;
  message += "/";
  message += NewAngleStepPosition;
  mqttDebug(message.c_str(), __LINE__, __FILE__);
  /*
   *Get steps from last known
   */
  long RotationMoveCount = RotationStepNow - NewRotationStepPosition;
  long AngleMoveCount = AngleStepNow - NewAngleStepPosition;

  // If neg then move backwards and remove the -
  // Compute the Rotation Steps needed
  if (RotationMoveCount < 0) {
    digitalWrite(ROTATION_STEP_DIR, LOW);
    RotationMoveCount = RotationMoveCount * -1;
    mqttDebug("Rotation Reverse", __LINE__, __FILE__);
  } else {
    digitalWrite(ROTATION_STEP_DIR, HIGH);
    mqttDebug("Rotation Forward", __LINE__, __FILE__);
  }

  yield();

  long xCount = 0;

  mqttDebug("+ Rotation Start", __LINE__, __FILE__);
  while (xCount < RotationMoveCount) {

    digitalWrite(ROTATION_STEP_PIN, HIGH);
    delayMicroseconds(470);
    vTaskDelay(1 / portTICK_RATE_MS);
    digitalWrite(ROTATION_STEP_PIN, LOW);
    delayMicroseconds(470);
    // vTaskDelay(470 / portTICK_RATE_MS);
    yield();
    xCount++;
    //    mqttLoop();
  }

  //  mqttLoop();
  mqttDebug("+ Rotation End", __LINE__, __FILE__);
  xCount = 0;

  // bool ReversAngle = false;
  if (AngleMoveCount < 0) {
    digitalWrite(ANGLE_STEP_DIR, HIGH);
    AngleMoveCount = AngleMoveCount * -1;
    mqttDebug("Angle Reverse", __LINE__, __FILE__);
    //  ReversAngle = true;
  } else if (AngleMoveCount > 0) {
    digitalWrite(ANGLE_STEP_DIR, LOW);
    mqttDebug("Angle Forward", __LINE__, __FILE__);
  } else {
    mqttDebug("No Angle needed", __LINE__, __FILE__);
  }

  /*
   * Stop at end of move or limit reached unless move is in
   * reverse then limit is not relevent
   */
  mqttDebug("+ Angle Start", __LINE__, __FILE__);
  while (xCount < AngleMoveCount) {

    digitalWrite(ANGLE_STEP_PIN, HIGH);
    delayMicroseconds(1870);
    vTaskDelay(1 / portTICK_RATE_MS);
    digitalWrite(ANGLE_STEP_PIN, LOW);
    delayMicroseconds(1870);
    yield();
    xCount++;
    //  mqttLoop();
  }
  mqttDebug("+ Angle End", __LINE__, __FILE__);
  // CommandOff();
  message = "Move done with steps:";
  message += RotationMoveCount;
  message += "/";
  message += AngleMoveCount;
  mqttDebug(message.c_str(), __LINE__, __FILE__);
  /****
   * Only save for non trim operations
   ****/
  if (!isTrim) {
    SaveRotationStepPosition(NewRotationStepPosition);
    SaveAngleStepPosition(NewAngleStepPosition);
  }
  return true;
}

void MotorMessage(cmdRecieved CMD, float Rotation, float Angle) {
  if (xMotorQueue == NULL) {
    SerialOut("Motor Queue queue not ready");
    return;
  }

  motorMessage msg;
  msg.MessageID = CMD;
  msg.ucAngle = Angle;
  msg.ucRotation = Rotation;
  xQueueSend(xMotorQueue, (void *)&msg, 0);
}

void motorTask(void *pvParameter) {

  while (xMotorQueue == NULL) {
    SerialOut("Motor Queue Task not ready");
    vTaskDelay(2000 / portTICK_RATE_MS);
  }

  struct motorMessage msg;

  for (;;) {
    if (xQueueReceive(xMotorQueue, &(msg),
                      (TickType_t)(2000 / portTICK_RATE_MS)) == pdPASS) {
      cmdLED_on();
      if (msg.MessageID == cmdMOVE) {
        mqttAction("+++++Q:Move START");
        Move(msg.ucRotation, msg.ucAngle, false, false);
        mqttAction("+++++Q:Move END");
      } else if (msg.MessageID == cmdHOME) {
        mqttAction("+++++Q:Home START");
        xHome();
        mqttAction("+++++Q:Home END");
      } else if (msg.MessageID == cmdSTEP) {
        mqttAction("+++++Q:Step START");
      } else if (msg.MessageID == cmdTRIM) {
        mqttAction("+++++Q:Trim START");
        Move(msg.ucRotation, msg.ucAngle, false, true);
        mqttAction("+++++Q:Trim END");
      } else {
        mqttDebug("+++++ Q:Unknown Command", __LINE__, __FILE__);
      }
      OutputTaskData();
      cmdLED_off();
    }
    vTaskDelay(2000 / portTICK_RATE_MS);
  }
}
